package com.answer.tostitos.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.answer.tostitos.R;
import com.answer.tostitos.databinding.ActivityResultBinding;

public class Result_Activity extends AppCompatActivity {

    private ActivityResultBinding binding;
    private String correctAnswer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityResultBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        correctAnswer = getIntent().getStringExtra("correctAnswer");

        if (correctAnswer.equals("4")){
            binding.circleResult.setImageResource(R.drawable.pass_circle);
            binding.resultText.setVisibility(View.GONE);
        } else {
            binding.resultText.setText("HAS ACERTADO " + correctAnswer + " DE 4");
            binding.circleResult.setImageResource(R.drawable.fail_circle);
            binding.failImgText.setVisibility(View.VISIBLE);
        }

    }


}