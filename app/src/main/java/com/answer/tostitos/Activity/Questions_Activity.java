package com.answer.tostitos.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.answer.tostitos.Adapter.Days_Preferred_Adapter;
import com.answer.tostitos.Utils.OnItemClickListener;
import com.answer.tostitos.databinding.ActivityQuestionsBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class Questions_Activity extends AppCompatActivity {

    private ActivityQuestionsBinding binding;
    private Days_Preferred_Adapter adapter;

    private JSONArray data;
    private int questionPos = 0;
    private CountDownTimer timer;
    private boolean isSelect = false;
    private int correctAnswer = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityQuestionsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.recAnswer.setLayoutManager(new LinearLayoutManager(this));


        int resID = getResources().getIdentifier("que1_img", "drawable", getPackageName());
        binding.bgImage.setImageResource(resID);


        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());

            data = obj.getJSONArray("data");

            binding.questionTV.setText(data.getJSONObject(questionPos).getString("question"));

            adapter = new Days_Preferred_Adapter(Questions_Activity.this,
                    data.getJSONObject(questionPos).getJSONArray("answer"), new OnItemClickListener() {
                @Override
                public void onItemClick(int item) throws JSONException {

                    if (!isSelect) {
                        if (data.getJSONObject(questionPos).getString("right_ans")
                                .equals((data.getJSONObject(questionPos).getJSONArray("answer")
                                        .getJSONObject(item).getString("Alphabet")))){
                            correctAnswer++;
                        }

                        JSONObject object = data.getJSONObject(questionPos).getJSONArray("answer")
                                .getJSONObject(item);
                        object.remove("isCheck");
                        object.put("isCheck", "true");
                        adapter.notifyDataSetChanged();
                        changeQuestion();
                    }

                }
            });
            binding.recAnswer.setAdapter(adapter);
            adapter.notifyDataSetChanged();


        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        createTimer();
        timerStart();

    }

    public void changeQuestion() {
        timerStop();
        if (questionPos >= 3){
            startActivity(new Intent(Questions_Activity.this, Result_Activity.class)
                    .putExtra("correctAnswer", String.valueOf(correctAnswer)));
            finish();
        }

        if (data.length() > (questionPos + 1)) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    questionPos++;
                    try {
                        binding.questionTV.setText(data.getJSONObject(questionPos).getString("question"));
                        binding.bgImage.setImageResource(getResources()
                                .getIdentifier(data.getJSONObject(questionPos).getString("bg_image")
                                        , "drawable", getPackageName()));

                        adapter.updateData(data.getJSONObject(questionPos).getJSONArray("answer"));
                        timerStart();

                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }
            }, 0);
        }
    }

    public void createTimer() {
        timer = new CountDownTimer(13000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                binding.timerTV.setText("Tiempo Restante: " + millisUntilFinished / 1000 + "s");
            }

            @Override
            public void onFinish() {
                changeQuestion();
            }
        };
    }

    public void timerStart() {
        if (timer != null) {
            timer.start();
            isSelect = false;
        }
    }

    public void timerStop() {
        if (timer != null) {
            timer.cancel();
            isSelect = true;
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("question_json.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onResume() {
        super.onResume();
        timerStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        timerStop();
    }

}