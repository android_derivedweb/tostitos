package com.answer.tostitos.Utils;

public class Constant_ad {


    public static String IS_LOGIN = "is_login" ;
    public static String FIREBASE_TOKEN = "firebase_token";
    public static String FULL_NAME = "full_name";
    public static String PHONE_NUMBER = "phone_number";
    public static String EMAIL = "email";
    public static String USER_ID = "user_id";
    public static String GOOGLE_ID = "google_id";
    public static String API_TOKEN = "api_token";

}
