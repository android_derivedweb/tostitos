package com.answer.tostitos.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.answer.tostitos.Activity.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility {

    public static String trimMessage(String json, String key) {
        String trimmedString = null;
        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }

    public static String getTime(String mTime) {
        Date dt;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
            dt = sdf.parse(mTime);
            return "" + sdfs.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getHour(String mTime) {
        Date dt;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            SimpleDateFormat sdfs = new SimpleDateFormat("HH");
            dt = sdf.parse(mTime);
            return "" + sdfs.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMinutes(String mTime) {
        Date dt;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            SimpleDateFormat sdfs = new SimpleDateFormat("mm");
            dt = sdf.parse(mTime);
            return "" + sdfs.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTimeFromDateTime(String mTime) {
        if (mTime.equals("null")){
            return "null";
        }
        Date dt;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
            dt = sdf.parse(mTime);
            return "" + sdfs.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDate(String mDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(mDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void logOut(Context context){
        SharePrefUtils.clearAll();
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

}
