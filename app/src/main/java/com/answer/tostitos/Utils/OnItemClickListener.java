package com.answer.tostitos.Utils;

import org.json.JSONException;

public interface OnItemClickListener {
    void onItemClick(int item) throws JSONException;
}