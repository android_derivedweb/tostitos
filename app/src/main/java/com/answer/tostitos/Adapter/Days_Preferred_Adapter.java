package com.answer.tostitos.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.answer.tostitos.Utils.OnItemClickListener;
import com.answer.tostitos.databinding.ItemAnswerBinding;

import org.json.JSONArray;
import org.json.JSONException;


public class Days_Preferred_Adapter extends RecyclerView.Adapter<Days_Preferred_Adapter.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private JSONArray array;

    private int selectedPosition = -1;

    public Days_Preferred_Adapter(Context mContext, JSONArray array, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.array = array;
        this.listener = onItemClickListener;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Viewholder(ItemAnswerBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    listener.onItemClick(position);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

//                if (selectedPosition >= 0) {
//                    notifyItemChanged(selectedPosition);
//                }
//                selectedPosition = holder.getAdapterPosition();
//                notifyItemChanged(selectedPosition);


//                 if (!prefModelArrayList.get(position).isSelected()) {
//                     holder.daysPreferredBinding.dayName.setTextColor(mContext.getResources().getColor(R.color.white));
//                     holder.daysPreferredBinding.relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.btn_round_black_3dp));
//                 } else {
//                     holder.daysPreferredBinding.dayName.setTextColor(mContext.getResources().getColor(R.color.black));
//                     holder.daysPreferredBinding.relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.btn_round_white_3dp));
//                 }
            }
        });

//         int resID = mContext.getResources().getIdentifier("que1_img", "drawable",  mContext.getPackageName());


        try {
            if (array.getJSONObject(position).getString("isCheck").equals("true")) {
                holder.itemAnswerBinding.answerImage.setImageResource(mContext.getResources()
                        .getIdentifier(array.getJSONObject(position).getString("AnsUnCheck"), "drawable", mContext.getPackageName()));
            } else {
                holder.itemAnswerBinding.answerImage.setImageResource(mContext.getResources()
                        .getIdentifier(array.getJSONObject(position).getString("AnsCheck"), "drawable", mContext.getPackageName()));
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public int getItemCount() {
        return array.length();
    }

    public void updateData(JSONArray array) {
        this.array = array;
        selectedPosition = -1;
        notifyDataSetChanged();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ItemAnswerBinding itemAnswerBinding;

        public Viewholder(ItemAnswerBinding binding) {
            super(binding.getRoot());
            itemAnswerBinding = binding;

        }
    }

}
